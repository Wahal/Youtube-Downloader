import youtube_dl, os

print ()
print ('-'*50)
print ('The Dyna Youtube Downloader')
print ('-'*50)
print () 

os.system('pip install youtube_dl')

url = str(input('\nEnter URL: '))
print ()

options = {
    'format' : 'bestaudio/best',
    'extractaudio' : True,      # only keep the audio
    'audioformat' : "mp3",      # convert to mp3
    'outtmpl': '%(id)s',        # name the file the ID of the video
    'noplaylist' : True,        # only download single song, not playlist
}

try:
    with youtube_dl.YoutubeDL(options) as ydl:

        r = ydl.extract_info(url, download = True)
        print ("\n%s was uploaded by '%s' and has %d views, %d likes, and %d dislikes.\n"\
               % (r['title'], r['uploader'], r['view_count'],r['like_count'], r['dislike_count']))
        os.rename(r['id'],r['title'] + '.mp3')
        print ('Download Complete.')
        print ('\nNOTE: If your MP3 file does not works, kindly try it with VLC.\n')
        x = input('Press any key to exit...')
        
except KeyboardInterrupt: print ("Download Failed. User interrupted the download.")

